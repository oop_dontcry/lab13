package com.paowaric.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.temporal.JulianFields;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class HelloMyName extends JFrame{
    JLabel lblName;
    JTextField textName;
    JButton btnHello;
    JLabel lblHello;
    public HelloMyName() {
        super("Hello My Name");
        lblName = new JLabel("Name: ");
        lblName.setBounds(10,10,50,20);
        lblName.setHorizontalAlignment(JLabel.RIGHT);

        textName = new JTextField();
        textName.setBounds(70,10,250,20);

        btnHello = new JButton("Hello");
        btnHello.setBounds(70,40,200,20);
        btnHello.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String myName = textName.getText();
                lblHello.setText("Hello " + myName);
            }

        });

        lblHello = new JLabel("Hello!! What is Your Name?");
        lblHello.setHorizontalAlignment(JLabel.CENTER);
        lblHello.setBounds(30,70,250,20);

        this.add(textName);
        this.add(lblName);
        this.add(btnHello);
        this.add(lblHello);

        this.setLayout(null);
        this.setSize(400,300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    public static void main(String[] args) {
        HelloMyName frame = new HelloMyName();
    }
}

  
